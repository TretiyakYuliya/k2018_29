﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MyCalcLib.Tests
{
    [TestFixture]
    public class CalcTest
    {
        [Test]
        public void Sum_10and5_15returned()
        {
            int x = 10;
            int y = 5;
            int expected = 15;
            Calc c = new Calc();
            int actual = c.Sum(x, y);
            Assert.AreEqual(expected, actual);
              
        }
        [Test]
        public void Reduct_15and5_10returned()
        {
            int x = 15;
            int y = 5;
            int expected = 10;
            Calc c = new Calc();
            int actual = c.Reduct(x, y);
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void Mult_5and5_25returned()
        {
            int x = 5;
            int y = 5;
            int expected = 20;
            Calc c = new Calc();
            int actual = c.Mult(x, y);
            Assert.AreEqual(expected, actual);

        }
        [Test]
        public void Div_10and5_5returned()
        {
            int x = 10;
            int y = 5;
            int expected = 5;
            Calc c = new Calc();
            int actual = c.Div(x, y);
            Assert.AreEqual(expected, actual);

        }


    }
}
